stages:
  - Dispatch
  - Finalize

# Control how the whole ci/cd system runs. At least one rule must evaluate
# to true or nothing will be excuted.
workflow:
  rules:
    # CI_COMMIT_BEFORE_SHA reprents the previous latest commit present on a branch
    # prior to a commit being pushed. In cases where no value can be known, gitlab
    # sets this to 0000000000000000000000000000000000000000. Some cases: the first
    # push of a new branch and all merge request pipelines (a merge request pipeline
    # is triggered for a source branch every time a commit lands on the destination
    # branch of the merge request).
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BEFORE_SHA == "0000000000000000000000000000000000000000"
      variables:
        # On the first push of a new branch, gitlab marks all files changed because
        # there is no explicit source to compare against. For our use-cases, this
        # incorrectly triggers every job in the repository that watches for specific
        # files to change. This variable is used to mitigate that edge case.
        # /ref: https://gitlab.com/gitlab-org/gitlab/-/issues/369916
        FIRST_PUSH_FOR_NEW_BRANCH: "true"
        # TODO: some detection of force pushes is needed as well (pipelines behave
        # in unintelligible ways due to incorrect handling of changed files).
        # /ref: https://gitlab.com/gitlab-org/gitlab/-/issues/409307
    # If merge pipelines are desired, uncomment the lines below. Discussion about
    # the purpose of said pipelines is needed first--this will break CI in very
    # confusing ways until those things have been considered.
    #- if: $CI_MERGE_REQUEST_IID   # Allow if commit has merge request.
    #- if: $CI_OPEN_MERGE_REQUESTS # Prevent branch pipeline if it has
    #  when: never                 # an open merge request.
  
    # Allow branch pipelines.
    - if: $CI_COMMIT_BRANCH

include:
  - shared/gitlab/*.yml
  - "**/*/.gitlab-ci.yml"

render:
  stage: Dispatch
  image: alpine/k8s:1.24.13
  needs: []
  variables:
    PROJECT_NAME: app-one
    PROJECT_PATH: projects/app-one
  parallel:
    matrix:
      - ENVIRONMENT: [prod, dev, ephemeral]
  script:
    - export JOB_NAME="$PROJECT_NAME/$ENVIRONMENT"
    - export PROJECT_NAME ENVIRONMENT
    - mkdir -p pipelines
    - cat shared/gitlab/template/deployment.yml | envsubst '$JOB_NAME $PROJECT_NAME $ENVIRONMENT' | tee -a /dev/stderr > pipelines/${ENVIRONMENT}.yml
  artifacts:
    paths: [pipelines/**/*]
    
dispatch/ephemeral:
  stage: Dispatch
  needs: [render]
  trigger:
    strategy: depend
    include:
    - artifact: pipelines/ephemeral.yml
      job: "render: [ephemeral]"


dispatch/dev:
  stage: Dispatch
  needs: [render]
  trigger:
    strategy: depend
    include:
    - artifact: pipelines/dev.yml
      job: "render: [dev]"

dispatch/prod:
  stage: Dispatch
  needs: [render]
  trigger:
    strategy: depend
    include:
    - artifact: pipelines/prod.yml
      job: "render: [prod]"


# Remove errant wip files pesent in project directories when commits land on main.
# Work in progress files are used in feature branches to signal that CI should
# deploy a project a developer needs but is not necessarily actively developing.
unwip:
  stage: Finalize
  image: alpinelinux/alpine-gitlab-ci
  script:
    - export GIT_SSL_CAINFO=$CI_SERVER_TLS_CA_FILE
    - rm projects/**/wip
    - git config --global user.email "git@gitlab.com"
    - git config --global user.name "CI Bot"
    - git config --global --add safe.directory $CI_PROJECT_DIR
    - git add -A
    - git commit -m "[ci] remove wip files"
    - git remote remove gitlab || true # local repo state may be cached
    - git remote add gitlab "https://gitlab-ci-token:$PROJECT_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git"
    - git push gitlab HEAD:main -o ci.skip
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
      exists: 
        - projects/**/wip